-- added at the users table

INSERT INTO users (email, password, datetime_posted) VALUES ("johnsmith@gmail.com", "passwordB", '2021-02-01 01:00:00')

INSERT INTO users (email, password, datetime_posted) VALUES ("juandelacruz@gmail.com", "passwordB", '2021-02-01 02:00:00');

INSERT INTO users (email, password, datetime_posted) VALUES ("janesmith@gmail.com", "passwordC", '2021-02-01 03:00:00');

INSERT INTO users (email, password, datetime_posted) VALUES ("mariadelacruz@gmail.com", "passwordD", '2021-02-01 04:00:00');


-- Added to posts table

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ( 1, "First Code", "Hello World!", '2021-01-02 01:00:00');

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ( 1, "Second Code", "Hello Earth!", '2021-01-02 02:00:00');


INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ( 2, "Third Code", "Welcome to Mars!", '2021-01-02 03:00:00');

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES ( 2, "Fourt Code", "Bye bye solar system!", '2021-01-02 04:00:00');

-- Get all post with author id of 1


SELECT * FROM posts WHERE author_id = 1;

-- Get all user's email and datetime of posted


SELECT email, datetime_posted FROM users;


