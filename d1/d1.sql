-- Inserting/Creating

INSERT INTO artist (name) VALUES ("Rivermaya");
INSERT INTO artist (name) VALUES ("Psy");


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-01-01", 2 );
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1995-01-01", 1 );


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 234, "OPM", 2 );
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 123, "OPM", 2 );


-- Selecting/Retrieving Records

-- Display all columns for all songs
SELECT * FROM songs;

-- Display the title and genre of all songs
SELECT song_name, genre FROM songs;

-- Display the title of all songs
SELECT song_name FROM songs WHERE genre = "OPM";


-- Display title and length of OPM songs that are longer than 2:10 sec

SELECT song_name, length FROM songs WHERE length > 240 and genre = "OPM";

-- Updating records
-- Removing the WHERE clause will update All row
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";


-- Dleting Records
-- Removing the WHERE clause will delete All row
DELETE FROM songs WHERE genre = "OPM" and length > 240;



